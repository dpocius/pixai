import { Component, OnInit } from '@angular/core';
import { InfoService } from './../info.service';
import { ActivatedRoute, ParamMap } from "@angular/router";

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {
	public infoservice;
  constructor(private infoService: InfoService, private route: ActivatedRoute) { }

  ngOnInit() {
	  this.infoservice = this.infoService;
	  var th = this.infoservice;
	  this.route.paramMap.subscribe((params: ParamMap) => {
		 var skiph = 0;
		 if (params.get("skip") != null) {
			 skiph = parseInt(params.get("skip"));
		 }
		 th.setAndLoad(skiph);
	  });
	  
  }

}
