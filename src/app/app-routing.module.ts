import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { MainComponent } from './main/main.component';
import { AboutComponent } from './about/about.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { DescComponent } from './desc/desc.component';
import { ListingComponent } from './listing/listing.component';

const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'desc', component: DescComponent },
  { path: 'show', component: ListingComponent },
  { path: 'show/:skip', component: ListingComponent },
  { path: 'about', component: AboutComponent },
  { path: 'terms-and-conditions', component: TermsComponent },
  { path: 'privacy-policy', component: PrivacyComponent },
  { path: 'entry/:x/:y', component: CreateComponent },
  { path: 'entry/:x/:y/:info', component: CreateComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
