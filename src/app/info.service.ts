import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InfoService {
  public pixels;
  public allupixels;
  public loadUsedPixels;
  public imgmain;
  public skipset;
  public doloading;
  constructor() { this.doloading = false; }
  
  loadpixelsus(cpixels,img) {
	  this.imgmain = img;
	  this.loadUsedPixels = cpixels;
  }
  setAndLoad(howmskip) {
	  this.skipset = howmskip;
	  this.doloading = true;
	  this.doLoadUsed();
  }
  doLoadUsed() {
	  if (typeof this.doloading != "undefined" && this.doloading != null && this.doloading != "" && this.doloading && typeof this.loadUsedPixels != "undefined" && 
	  this.loadUsedPixels != null && this.loadUsedPixels != "") {
		  var maxShowOneTime = 100;
		  var howmskip = this.skipset*maxShowOneTime;
		  let img = this.imgmain;
		  let cpixels = this.loadUsedPixels;
		let pixn = cpixels.length;
		let emptpixels = 0;
		
		let pixelsused = [];
		let skippednow = 0;
		let allusedpx = 0;
		
		for (let i = 0; i < pixn; i += 4) {
		  if (cpixels[i] == 255 && cpixels[i + 1] == 255 && cpixels[i + 2] == 255) {
			  emptpixels = emptpixels + 1;
		  }else{
			  var x = (i / 4) % img.width;
			  var y = Math.floor((i / 4) / img.width);
			  allusedpx = allusedpx+1;
			  if (maxShowOneTime > pixelsused.length) {
				if (howmskip > skippednow) {
					skippednow = skippednow+1;
				} else {
					pixelsused.push({x: x, y: y});
				}
			  }
		  }
		}
		this.setAllUsedPixels(Math.floor(allusedpx/maxShowOneTime));
		this.setUsedPixels(pixelsused);
	}
  }
  setUsedPixels(pixel) {
	  this.pixels = pixel;
  }
   setAllUsedPixels(allupixs) {
	  this.allupixels = Array(allupixs).fill(0).map((x,i)=>i+1);
  }
  
  getUsedPixel(){
	  return this.pixels;
  }  
  getAllUsedPixel(){
	  return this.allupixels;
  }
}
