import { Component } from '@angular/core';
import * as $ from 'jquery';
import { Router } from "@angular/router";
import { MainComponent } from './main/main.component';
import {FormsModule} from '@angular/forms'
import { InfoService } from './info.service';


@Component({
  providers:[MainComponent],
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pixel';
  constructor(private router: Router, private main: MainComponent, private infoService: InfoService){}
  public pixels = [];
  public mains = [];
  public mainserr = [];
  public zoomprop = 1;
  public wouldcost = 1;
  
	public isimageloading = false;
	public mouseMoving = false;
	public canvasContext;
	public emptycolor = 255;
	public drawingArea = { w:0, h:0, startX:0, startY:0  };
	public drawingSurfaceImageData;
	public popupMenu;
	public dragging = false;
    public canvas;
	public colorbefore;
	public notremoved = false;
	public show = false;
	public emptpixels = 0;
	public pixelsused = [];
	public x = -1;
	public y = -1;
	public skipshow = 0;
	public exist = "Spot is empty";
	public maincolor = "#f7659a";
	public selectedurlimage;
	public loadingadd = false;
	public COOKIE_CONSENT = "cookie_accepted_here"
	
  ngOnInit() {
	 this.loadingadd = false;
	 this.isimageloading = false;
	  this.isConsented = this.getCookie(this.COOKIE_CONSENT) === '1';
	this.resizeCanvas();
	window.addEventListener('resize', function(){
		this.resizeCanvas();
	}.bind(this), false);
			window["tinyMCE"].init({selector:'textarea'});
			this.uploadfile();
  }
  
  loadimg(src, callback) {
	var img = new Image;
	img.crossOrigin = "Anonymous";

	img.onload = function(){
		
		var canvas = document.createElement('canvas');
		canvas.width = img.width;
		canvas.height = img.height;
		canvas.getContext('2d').drawImage(img, 0, 0, img.width, img.height);
		let x = 0;
		let y = 0;
		
	  callback(img, canvas.getContext('2d'));
	};
	img.src = src;
  }
  
  resizeCanvas() {
	  this.loadimg("/pixi/img.php", (img, canvas) => {
		 this.resizeCanvassss(img, canvas);
	  });
  }  
  
  resizeCanvasimg(imgz) {
	  this.loadimg(imgz, (img, canvas) => {
		 this.resizeCanvassss(img, canvas);
	  });
  }
  
  removeimage(item) {
	let newarr = [];
	for (var i=0; i < this.mains.length; i++) {
		if (item != this.mains[i]) {
			newarr.push(this.mains[i]);
		}
	}
	this.mains = newarr;
  };

	useImageAsMain(img) {
		this.resizeCanvasimg("/pixi/"+img);
	};
	
	uploadInstantly2(e) {
		let th = this;
		this.isimageloading = false;
		console.log("th",e);
		let files = e.target.files[0];
		if (typeof files != "undefined") {
			e.stopPropagation();
			e.stopImmediatePropagation();
			this.isimageloading = true;
			var file_data = files;//jQuery("#sortpicture").prop("files")[0];   
			var form_data = new FormData();
			form_data.append("file", file_data);
			
			jQuery.ajax({
				url: "/pixi/upload.php",
				dataType: 'text',  // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,                         
				type: 'post',
				success: function(res){
					let json = JSON.parse(String(res));
					if (json.success) {
						th.mains.push(json.uploaded);
					}else{
						th.mainserr.push({err: json.error, name: json.name});
					}
					th.isimageloading = false;
				},
				  error: function (err) {
					th.mainserr.push({err: "File not uploaded. May be wrong extension or file is too big. Max - 10MB", name: file_data.name});
					th.isimageloading = false;
				  }
			});
		}
	}
	
	uploadfile() {
		let th = this;
		jQuery("#upload2").on("click", function(e) {
			e.stopPropagation();
			e.stopImmediatePropagation();
			var file_data = jQuery("#sortpicture2").prop("files")[0]; 
console.log("file_data",file_data);			
			var form_data = new FormData();
			form_data.append("file", file_data);
			
			jQuery.ajax({
				url: "/pixi/upload.php",
				dataType: 'text',  // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,                         
				type: 'post',
				success: function(res){
					let json = JSON.parse(String(res));
					if (json.success) {
						th.mains.push(json.uploaded);
						//th.resizeCanvasimg("/pixi/"+json.uploaded);
					}else{
						th.mainserr.push({err: json.error, name: json.name});
					}
				}
			});
		});
	};
	
	  addMoment() {
		  let rgba = this.hextorgb(this.maincolor);
		  var content =  window['tinyMCE'].get('maintext2').getContent();
		  let title = jQuery("#title2").val();
		  console.log(rgba, content, title);
		  let selectimg = "";
	
		  if (typeof this.selectedurlimage != "undefined" && this.selectedurlimage != ""){
			selectimg = this.selectedurlimage;
		  }else{
			  selectimg = "";
		  }
		  
		  this.setpixels(this.drawingArea.startX, this.drawingArea.startY, rgba, content, title, this.drawingArea.w, this.drawingArea.h, selectimg);
	  };
	  
	  setpixels(x, y, rgba, maintext, title, width, height, mainimg) {
		   this.loadingadd = true;
		   
		  let th = this;
		 for (var i=0; i < this.mains.length; i++) {
			maintext = maintext+" <img src='/pixi/"+this.mains[i]+"' alt='' />";
		}
		this.mains = [];
					
		jQuery.post( "/pixi/addtemp.php", {
			  data: maintext,
			  title: title,
			  x: x,
			  y: y,
			  r: rgba.r,
			  g: rgba.g,
			  b: rgba.b,
			  a: rgba.a,
			  width: width,
			  height: height,
			  mainimg: mainimg,
			  item_amount: width*height
			  })
		.done(function( data ) {
			th.loadingadd = false;
			let dataparse = JSON.parse(data);
			
			if (dataparse.success) {
				location.href = dataparse.urltogo;
			}
		});
	  };
  
  
  
    public isConsented: boolean = false;

    private getCookie(name: string) {
        let ca: Array<string> = document.cookie.split(';');
        let caLen: number = ca.length;
        let cookieName = `${name}=`;
        let c: string;

        for (let i: number = 0; i < caLen; i += 1) {
            c = ca[i].replace(/^\s+/g, '');
            if (c.indexOf(cookieName) == 0) {
                return c.substring(cookieName.length, c.length);
            }
        }
        return '';
    }

    private deleteCookie(name) {
        this.setCookie(name, '', -1);
    }

    private setCookie(name: string, value: string, expireDays: number, path: string = '') {
        let d:Date = new Date();
        d.setTime(d.getTime() + expireDays * 24 * 60 * 60 * 1000);
        let expires:string = `expires=${d.toUTCString()}`;
        let cpath:string = path ? `; path=${path}` : '';
        document.cookie = `${name}=${value}; ${expires}${cpath}`;
    }

    public consent(isConsent: boolean, e: any) {
            this.setCookie(this.COOKIE_CONSENT, '1', 356);
            this.isConsented = true;
            e.preventDefault();
    }
  
  acceptcookies(e: any) {
	  this.consent(true, e);
  };
  
  resizeCanvassss(img, colorscanvas) {
	let cont : any = jQuery("#canvascontainer");
	let canvas : any = document.getElementById("myCanvas");
	this.canvas = canvas;
	
	let ctx = canvas.getContext("2d");
	this.canvasContext = ctx;
							
	
	ctx.drawImage(img,0,0, img.width, img.height);

	let cpixels = this.canvasContext.getImageData(0, 0, img.width, img.height).data;
	this.emptpixels = 0;
	let pixn = cpixels.length;
	var maxShowOneTime = 50;
	this.pixelsused = [];
	let howmskip = this.skipshow;
	let skippednow = 0;
	for (let i = 0; i < pixn; i += 4) {
	  if (cpixels[i] == 255 && cpixels[i + 1] == 255 && cpixels[i + 2] == 255) {
		  this.emptpixels = this.emptpixels + 1;
	  }
	}
	
	this.infoService.loadpixelsus(cpixels,img);
	this.infoService.doLoadUsed();

	this.colorbefore = "";
	let pixelsbefore = {offsetX: 0, offsetY: 0};
	
    canvas.addEventListener('click', function(e) {
        console.log('click: ' + e.offsetX + '/' + e.offsetY);
		th.show = false;
		th.router.navigateByUrl("/entry/"+e.offsetX+"/"+e.offsetY);
    }, false);
	
	let th = this;
					cont.on("mousemove", function(e) {
						th.x = (e.offsetX);
						th.y = (e.offsetY);
						if (th.dragging) {
							th.notremoved = true;
							th.mouseMoving = true;
							th.restoreDrawingSurface();
							
							var scrollTop = jQuery('#canvascontainer').find("#mCSB_1_dragger_vertical").position().top;
							var scrollLeft = jQuery('#canvascontainer').find("#mCSB_1_dragger_horizontal").position().left;
							var heightas = jQuery('#canvascontainer').find("#mCSB_1_scrollbar_vertical").height();
							var widthas = jQuery('#canvascontainer').find("#mCSB_1_scrollbar_horizontal").width();

							let timesheight = 1018/heightas;
							let timeswidth = 1018/widthas;


							th.drawingArea.w = Math.floor(Number(e.pageX - this.offsetLeft + scrollLeft*timeswidth) - th.drawingArea.startX);
							th.drawingArea.h = Math.floor(Number(e.clientY - this.offsetTop + scrollTop*timesheight) - th.drawingArea.startY);
							
							th.canvasContext.setLineDash([5]);
							th.canvasContext.strokeStyle = "rgb(33, 133, 208)";
							th.canvasContext.strokeRect(th.drawingArea.startX, th.drawingArea.startY, th.drawingArea.w, th.drawingArea.h);
							th.canvasContext.setLineDash([]);
							th.canvasContext.fillStyle = "rgba(33, 133, 208, 0.2)";
							th.canvasContext.fillRect(th.drawingArea.startX, th.drawingArea.startY, th.drawingArea.w, th.drawingArea.h);
						
						//wouldcost
						let cntprice = 0;
							if (th.drawingArea.w > 0 && th.drawingArea.h > 0) {
								let cpixels = th.canvasContext.getImageData(th.drawingArea.startX, th.drawingArea.startY, th.drawingArea.w, th.drawingArea.h).data;
								for (let i = 0; i < cpixels.length; i += 4) {
								  if (cpixels[i] == 255 && cpixels[i + 1] == 255 && cpixels[i + 2] == 255) {
									  cntprice = cntprice + 1;
								  }
								}
							}
							
							th.wouldcost = cntprice;
						}else{
							if(th.notremoved){
								th.restoreDrawingSurface();
							}
							let pixelData = colorscanvas.getImageData(e.offsetX, e.offsetY, 1, 1).data;
							let color = "rgba("+pixelData[0]+","+pixelData[1]+","+pixelData[2]+","+pixelData[3]+")";
							
							if(pixelData[0] == th.emptycolor && pixelData[1] == th.emptycolor && pixelData[2] == th.emptycolor &&
							pixelData[3] == th.emptycolor){
								th.exist = "Spot is empty";
							}else{
								th.exist = "Check It";
							}
							//let pixelData = colorscanvas.getImageData(e.offsetX, e.offsetY, 1, 1).data;
							//let color = "rgba("+pixelData[0]+","+pixelData[1]+","+pixelData[2]+","+pixelData[3]+")";
							 //jQuery("#pxlook").css({"left": e.offsetX, "top": e.offsetY});
							//if(th.colorbefore != ""){
								
								//ctx.fillStyle = th.colorbefore;
								//ctx.fillRect(pixelsbefore.offsetX, pixelsbefore.offsetY, 1, 1);
								//th.colorbefore = color;
								//pixelsbefore = {offsetX: e.offsetX, offsetY: e.offsetY};
							//}else{
								//th.colorbefore = color;
								//pixelsbefore = {offsetX: e.offsetX, offsetY: e.offsetY};
							//}
							
							//ctx.fillStyle = "red";
							//ctx.fillRect(e.offsetX, e.offsetY, 1, 1);
						}
					});
					
					cont.on("mousedown", function(e) {
						if (e.button == 2) {
								th.mouseMoving = false;
								th.saveDrawingSurface();
							var scrollTop = jQuery('#canvascontainer').find("#mCSB_1_dragger_vertical").position().top;
							var scrollLeft = jQuery('#canvascontainer').find("#mCSB_1_dragger_horizontal").position().left;
							var heightas = jQuery('#canvascontainer').find("#mCSB_1_scrollbar_vertical").height();
							var widthas = jQuery('#canvascontainer').find("#mCSB_1_scrollbar_horizontal").width();

							let timesheight = 1018/heightas;
							let timeswidth = 1018/widthas;
							 
								th.drawingArea.startX = Math.floor(Number(e.pageX - this.offsetLeft + scrollLeft*timeswidth));
								th.drawingArea.startY = Math.floor(Number(e.clientY - this.offsetTop + scrollTop*timesheight));
								
								th.dragging = true;
								//container.style.cursor = "crosshair";
						}
						if(th.colorbefore != ""){
								ctx.fillStyle = th.colorbefore;
								ctx.fillRect(pixelsbefore.offsetX, pixelsbefore.offsetY, 1, 1);
							}
					}); 
					
					cont.on("mouseout", function(e) {
						th.x = -1;
						th.y = -1;
						if(th.notremoved){
							th.mouseMoving = false;
							th.restoreDrawingSurface();
							th.notremoved = false;
							th.dragging = false;
						}
					});
					cont.on("mouseup", function(e) {
						if (e.button == 2) {
							th.restoreDrawingSurface();
							th.notremoved = false;
							th.dragging = false;
							//container.style.cursor = "default";
							let xRange = th.getStartToEnd(th.drawingArea.startX, th.drawingArea.w);
							let yRange = th.getStartToEnd(th.drawingArea.startY, th.drawingArea.h);
							console.log("XRANGE", xRange);
							console.log("YRANGE", yRange);
							th.drawingArea.w = Math.floor(Number(-th.drawingArea.w>0 ? -(th.drawingArea.w) : (th.drawingArea.w)));
							th.drawingArea.h = Math.floor(Number(-th.drawingArea.h>0 ? -(th.drawingArea.h) : (th.drawingArea.h)));
							th.createFromToPixels(th.drawingArea);
							
							let cntprice = 0;
							if (th.drawingArea.w > 0 && th.drawingArea.h > 0) {
								let cpixels = th.canvasContext.getImageData(th.drawingArea.startX, th.drawingArea.startY, th.drawingArea.w, th.drawingArea.h).data;
								for (let i = 0; i < cpixels.length; i += 4) {
								  if (cpixels[i] == 255 && cpixels[i + 1] == 255 && cpixels[i + 2] == 255) {
									  cntprice = cntprice + 1;
								  }else{
									console.log("-aba aba aba", cntprice);
								  }
								}
							}
							
							th.wouldcost = cntprice;
							
							//if(mouseMoving) {
							//	selectNodesFromHighlight();
							//}
						}
						if(th.colorbefore != ""){
								ctx.fillStyle = th.colorbefore;
								ctx.fillRect(pixelsbefore.offsetX, pixelsbefore.offsetY, 1, 1);
							}
					});
	

	
	canvas.addEventListener('contextmenu', e => {
		if(th.colorbefore != ""){
								ctx.fillStyle = th.colorbefore;
								ctx.fillRect(pixelsbefore.offsetX, pixelsbefore.offsetY, 1, 1);
							}
		e.preventDefault();				
	});
	
  }
  
  
	    saveDrawingSurface = () => {
			this.drawingSurfaceImageData = this.canvasContext.getImageData(0, 0, this.canvas.width, this.canvas.height);
		};

	    restoreDrawingSurface = () => {
			this.canvasContext.putImageData(this.drawingSurfaceImageData, 0, 0);
		};

		selectNodesFromHighlight = () => {
			console.log("HEEEE");
		};

	    getStartToEnd = (start, theLen) => {
			return theLen > 0 ? {start: start, end: start + theLen} : {start: start + theLen, end: start};
		};
		
		zoomin = () => {
			this.zoomprop += 0.1;
			jQuery('#myCanvas').css("transform", "scale("+this.zoomprop+")");
			//jQuery('#myCanvas').css("-ms-zoom", this.zoomprop);
		};
		zoomout = () => {
			this.zoomprop -= 0.1;
			jQuery('#myCanvas').css("transform", "scale("+this.zoomprop+")");
			//jQuery('#myCanvas').css("-ms-zoom", this.zoomprop);
		};
		
		createFromToPixels = (data) => {
			if (this.drawingArea.w > 0 && this.drawingArea.h > 0){
				this.show = true;
				
				if (typeof this.selectedurlimage != "undefined" && this.selectedurlimage != null
				 && this.selectedurlimage != "") {
					this.addImageToContainer(this.selectedurlimage);
				}
			}
		}
		
		usemaincolorr = () => {
			this.selectedurlimage = "";
			let canvas : any = document.getElementById("drawingareahereuser");
			let ctx = canvas.getContext("2d");
			ctx.fillStyle = this.maincolor;
			ctx.fillRect(0, 0, canvas.width, canvas.height);
		}
		
		addImageToContainer = (imgg) => {
			
			this.selectedurlimage = imgg;
			if (this.drawingArea.w > 0 && this.drawingArea.h > 0){
			//	let imglink = "/pixi/img.php";
				let imglink = "/pixi/"+imgg;
				this.loadimg(imglink, (img, canva) => {

				let container : any = jQuery("#containerformaincanvas");
				let containercanvas : any = jQuery("#drawingareahereuser");
				let canvas : any = document.getElementById("drawingareahereuser");
				let canvascont : any = document.getElementById("containerformaincanvas");
				container.attr("width", this.drawingArea.w+'px');
				containercanvas.attr("width", img.width+'px');
				containercanvas.attr("height", img.height+'px');
				canvas.style.width = img.width+'px';
				canvascont.style.width = this.drawingArea.w+'px';
				canvascont.style.height = this.drawingArea.h+'px';
				container.attr("height",  this.drawingArea.h+'px');
				canvas.style.height = img.height+'px';
				
						let ctx = canvas.getContext("2d");
						
						ctx.drawImage(img,0,0, img.width, img.height);
						
						
					   var cpixels = ctx.getImageData(0, 0, this.drawingArea.w, this.drawingArea.h).data;
					   
					   //console.log("PIXEEEEEE", cpixels);
	//red=imgData.data[0];
	//green=imgData.data[1];
	//blue=imgData.data[2];
	//alpha=imgData.data[3];
		
				});
			}
		}
  hextorgb(hex) {
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        //return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',1)';
		let gcol = (c>>8)&255;
		return {r: (c>>16)&255, g: gcol, b: c&255, a: 0};
    }
    return hex;	  
  }
}
