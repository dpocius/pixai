import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, ParamMap } from "@angular/router";
import {FormsModule} from '@angular/forms'
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  public x;
  public y;
  public info = null;
  public showinfo = false;
  public isempty = true;
  public desc = "";
  public title = "";
  public likes = 0;
  public maincolor = "#f7659a";
  public mains = [];
  public mainserr = [];
  public loadingadd = false;
  public canaddlike = false;
  public isimageloading = false;
  constructor(private route: ActivatedRoute, private changeDetectorRef: ChangeDetectorRef, protected sanitizer: DomSanitizer) { }

  ngOnInit() {
	  this.loadingadd = false;
		this.route.paramMap.subscribe((params: ParamMap) => {
			this.loadingadd = false;
			this.x = params.get("x");
			this.y = params.get("y");
			this.info = params.get("info");

			if (this.info == "payed") {
				this.showinfo = true;
			}else{
				this.showinfo = false;
			}
			
			this.likes = 0;
			this.isimageloading = false;
			this.canaddlike = false;
			this.getlikes(this.x, this.y);
			this.getpixel(this.x, this.y);
			window["tinyMCE"].execCommand('mceRemoveEditor', false, 'maintext');
			window["tinyMCE"].init({selector:'textarea'});
			setTimeout(function() {
			window["tinyMCE"].init({selector:'textarea'});
			},100);
			this.uploadfile();
		});
  }
  
  getlikes(x, y) {
	  let getherecookie = this.getCookie("likescook"+x+y);
	  if (getherecookie == "") {
		this.canaddlike = true;
	  }
	  let th = this;
		jQuery.post( "/pixi/getlikes.php", {
			  pixelx: x,
			  pixely: y
			  })
		.done(function( data ) {
			if(data != ""){
				data = data.replace(/(\r\n|\n|\r)/gm, "");
				let parsejs = JSON.parse(data);
				th.likes = parsejs.likes;
			}
		});
		
  }
  
    private getCookie(name: string) {
        let ca: Array<string> = document.cookie.split(';');
        let caLen: number = ca.length;
        let cookieName = `${name}=`;
        let c: string;

        for (let i: number = 0; i < caLen; i += 1) {
            c = ca[i].replace(/^\s+/g, '');
            if (c.indexOf(cookieName) == 0) {
                return c.substring(cookieName.length, c.length);
            }
        }
        return '';
    }

    private setCookie(name: string, value: string, expireDays: number, path: string = '') {
        let d:Date = new Date();
        d.setTime(d.getTime() + expireDays * 24 * 60 * 60 * 1000);
        let expires:string = `expires=${d.toUTCString()}`;
        let cpath:string = path ? `; path=${path}` : '';
        document.cookie = `${name}=${value}; ${expires}${cpath}`;
    }
  
  likepix() {
	  this.setCookie("likescook"+this.x+this.y, '1', 356);
	  this.canaddlike = false;
	  let likess = +(this.likes);
	  this.likes = likess+1;
	  console.log("LIKES", this.x, this.y);
	  jQuery.post( "/pixi/updatelikes.php", {
			  pixelx: this.x,
			  pixely: this.y
			  })
		.done(function( data ) {
		});
  }
  
  getpixel(x, y) {
	  let th = this;
		jQuery.post( "/pixi/pixel.php", {
			  pixelx: x,
			  pixely: y
			  })
		.done(function( data ) {
			if(data != ""){
			data = data.replace(/(\r\n|\n|\r)/gm, "");
				let parsejs = JSON.parse(data);
				
				th.isempty = false;
				parsejs.desc = parsejs.desc.replace(/&quot;/g, '"');
				parsejs.desc = parsejs.desc.replace(/&lt;/g, '<');
				parsejs.desc = parsejs.desc.replace(/&gt;/g, '>');
				parsejs.desc = parsejs.desc.replace(/&quot;/g, '"');
				parsejs.desc = parsejs.desc.replace(/&apos;/g, "'");
				parsejs.desc = parsejs.desc.replace(/<script[^>]*>.*?<\/script>/gi,'');
				
				th.desc = th.sanitizer.bypassSecurityTrustHtml(parsejs.desc) as string;
				th.title = parsejs.title;
			}else{
				th.isempty = true;
			}
		});
  }
  removeimage(item) {
	let newarr = [];
	for (var i=0; i < this.mains.length; i++) {
		if (item != this.mains[i]) {
			newarr.push(this.mains[i]);
		}
	}
	this.mains = newarr;
  };

	uploadInstantly(e) {
		let th = this;
		this.isimageloading = false;
		console.log("th",e);
		let files = e.target.files[0];
		if (typeof files != "undefined") {
			e.stopPropagation();
			e.stopImmediatePropagation();
			this.isimageloading = true;
			var file_data = files;//jQuery("#sortpicture").prop("files")[0];   
			var form_data = new FormData();
			form_data.append("file", file_data);
			console.log("FILE DATA",file_data);
			jQuery.ajax({
				url: "/pixi/upload.php",
				dataType: 'text',  // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,                         
				type: 'post',
				success: function(res){
					let json = JSON.parse(String(res));
					if (json.success) {
						th.mains.push(json.uploaded);
					}else{
						th.mainserr.push({err: json.error, name: json.name});
					}
					th.isimageloading = false;
				},
				  error: function (err) {
					th.mainserr.push({err: "File not uploaded. May be wrong extension or file is too big. Max - 10MB", name: file_data.name});
					th.isimageloading = false;
				  }
			});
		}
	}
	uploadfile() {
		let th = this;
		jQuery("#upload").on("click", function(e) {
			e.stopPropagation();
			e.stopImmediatePropagation();
			var file_data = jQuery("#sortpicture").prop("files")[0];   
			var form_data = new FormData();
			form_data.append("file", file_data);
			
			jQuery.ajax({
				url: "/pixi/upload.php",
				dataType: 'text',  // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,                         
				type: 'post',
				success: function(res){
					let json = JSON.parse(String(res));
					if (json.success) {
						th.mains.push(json.uploaded);
					}else{
						th.mainserr.push({err: json.error, name: json.name});
					}
				}
			});
		});
	};
  
  addMoment() {
	  let rgba = this.hextorgb(this.maincolor);
	  var content =  window['tinyMCE'].get('maintext').getContent();
	  let title = jQuery("#title").val();
	  this.setpixel(this.x, this.y, rgba, content, title);
  };
  
  setpixel(x, y, rgba, maintext, title) {
	  this.loadingadd = true;
	  let th = this;
	 for (var i=0; i < this.mains.length; i++) {
		maintext = maintext+" <img src='/pixi/"+this.mains[i]+"' alt='' />";
	}
	this.mains = [];
	///pixi/change.php	
	jQuery.post( "/pixi/addtemp.php", {
		  data: maintext,
		  title: title,
		  x: x,
		  y: y,
		  r: rgba.r,
		  g: rgba.g,
		  b: rgba.b,
		  a: rgba.a,
		  width: 0,
		  height: 0,
		  mainimg: "",
		  item_amount: 1
		  })
	.done(function( data ) {
		th.loadingadd = false;
		let dataparse = JSON.parse(data); 
		
		if (dataparse.success) {
			location.href = dataparse.urltogo;
		}
	});
  }
  
  testchange(idtemp) {
	let th = this;

	jQuery.post( "/pixi/change.php", {
		  idtemp: idtemp
	})
	.done(function( data ) {
		console.log( "Data Loadeda: " + data );
	});
  }
  
  
  hextorgb(hex) {
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        //return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',1)';
		let gcol = (c>>8)&255;
		return {r: (c>>16)&255, g: gcol, b: c&255, a: 255};
    }
    return hex;	  
  }
  
}
