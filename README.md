# Pixel

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

1. Because of routes -> always redirect to index.html if not found file -> change nginx:
try_files $uri $uri /index.html;
2. Chmod pixi/bebe.png - because this file is changing
3. Chmod /pixi/pixi folder -> as upload goes there (if not exists pixi/pixi - create folder)

GD LIBRARY:
1. check your php version : php -v
2. Run one of the command according to your php verison
PHP5: sudo apt-get install php5-gd
PHP7.0: sudo apt-get install php7.0-gd
PHP7.1: sudo apt-get install php7.1-gd
PHP7.2: sudo apt-get install php7.2-gd
PHP7.3: sudo apt-get install php7.3-gd
PHP7.4: sudo apt-get install php7.4-gd
3. sudo apt-get install php-curl
