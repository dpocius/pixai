<?php

// PayPal settings
$paypal_email = 'dspocius@gmail.com';
$return_url = 'https://'+$_SERVER['HTTP_HOST']+'/ppaypal/payment-successful.html';
$cancel_url = 'https://'+$_SERVER['HTTP_HOST']+'/ppaypal/payment-cancelled.html';
$notify_url = 'https://'+$_SERVER['HTTP_HOST']+'/ppaypal/payment_succ_ipn.php';


// Include Functions
include("functions.php");

// Check if paypal request or response
if (!isset($_POST["txn_id"]) && !isset($_POST["txn_type"])){
	$querystring = '';
	
	// Firstly Append paypal account to querystring
	$querystring .= "?business=".urlencode($paypal_email)."&";
	
	// Append amount& currency (£) to quersytring so it cannot be edited in html
	
	//The item name and amount can be brought in dynamically by querying the $_POST['item_number'] variable.
	$querystring .= "item_name=".urlencode($_POST["item_name"])."&";
	$querystring .= "amount=".urlencode($_POST["item_amount"])."&";
	
	//loop for posted values and append to querystring
	foreach($_POST as $key => $value){
		$value = urlencode(stripslashes($value));
		$querystring .= "$key=$value&";
	}
	
	// Append paypal return addresses
	$querystring .= "return=".urlencode(stripslashes($return_url))."&";
	$querystring .= "cancel_return=".urlencode(stripslashes($cancel_url))."&";
	$querystring .= "notify_url=".urlencode($notify_url);
	
	// Append querystring with custom field
	$querystring .= "&custom="."VartotojoID";
	
	// Redirect to paypal IPN
	header('location:https://www.sandbox.paypal.com/cgi-bin/webscr'.$querystring);
	exit();
}
?>
